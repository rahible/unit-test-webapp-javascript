module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['qunit'],
        // all files needed for javascript testing
        files: [
            // third party first, order as includes appear on the page
            'target/dependency/META-INF/resources/webjars/jquery/3.6.0/jquery.js',
            // code to test
            'src/main/webapp/javascript/base-function.js',
            // test code
            'src/test/webapp/javascript/base-function.spec.js'
        ],
        // files to instrument for coverage
        preprocessors: {
            'src/main/webapp/javascript/base-function.js': 'coverage',
            'src/test/webapp/javascript/base-function.spec.js': 'coverage'
        },
        reporters: ['progress', 'coverage', 'junit'],
        autoWatch: true,
        browsers: ['FirefoxHeadless'],
        singleRun: true
    });
};