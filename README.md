# Testing Lose JavaScript Files in a Java Web Application

Some Java Applications, written as multi-page applications, years in the past, incorporated JavaScript as includes on a webpage
that was loaded from the base of the Application. Before modern JavaScript tools and frameworks made it easier to develop, testing
these JavaScript files usually was avoided.

Recently, I was working on updating the versions of JQuery in a similar application. Years ago, previous developers downloaded the
JQuery library and put it in the web directory of the applications source code. Ideally, I would like to have re-written the JavaScript
out of the pages and use npm to manage the JavaScript dependencies. However, like all things, time is money. 

I chose not only to follow a pattern like this to at least build out a set of tools that I could use to isolate only the changes I was going to make, but
also add unit tests to those changes. The first step was to remove the JQuery framework that had been included in the project. That was swapped out 
with webjars. The second was to use Qunit and Karma to run unit tests for the JavaScript. Finally, to extract the 3rd party libraries like
JQuery to allow Karma to find them, I used the maven-dependency-plugin.

This would be a good starting point for a larger refactoring to move to using node/npm to build out the JavaScript dependencies.

## Technologies

- [Qunit](https://qunitjs.com/)
- [Karma Test Runner](https://karma-runner.github.io/latest/index.html)
- [Maven](https://maven.apache.org/)
    - [maven-dependency-plugin](http://maven.apache.org/plugins/maven-dependency-plugin/), unpacks javascript from webjars
    - [exec-maven-plugin](https://www.mojohaus.org/exec-maven-plugin/)
    
## Technology Alternatives

Several technologies exist which test JavaScript using node and npm. I evaluated each by way of google searches and trial
and error. Below are things that I tried, which didn't seem to work for me.

### [Jest](https://jestjs.io/) 

- Advantages:
    - Testing framework, test runner and test coverage reports bundled together.
    - It is bundled with the AWS CDK, and I have some familiarity with it.
    - Uses sensible defaults and requires little configurations.
    - Testing syntax is like Jasmine and I have experience with it in Angular.
    
- Disadvantage:
    - The legacy JavaScript was not written with modules in mind. I wanted to avoid major refactoring if possible. Jest requires the
      functions to be exported.
      
### [Jasmine](https://jasmine.github.io/)

- Advantages:
    - I'm familiar with Jasmine.
    - The [jasmine-maven-plugin](https://searls.github.io/jasmine-maven-plugin/) was developed specifically for this type of testing and it supports webjars.
    
- Disadvantages:
    - Like Jest requires the code under test to support modules.
    - The [jasmine-maven-plugin](https://searls.github.io/jasmine-maven-plugin/) has not had a major release for a few years. The latest beta release is nearly a year old.
    
## How to Test

### Run Tests

As part of the maven build.

```
mvn clean test
```

As just a karma run.

```
karma start
```

### Add Tests

1. Identify the JavaScript file that contains the code that you want to test. If the JavaScript is on a JSP / HTML page, 
it will be required to extract the code into its own JavaScript file.
1. Create a mirrored JavaScript test file named to identify it as a testing file. Since this is a maven project, keeping
the tests in a mirror'd folder under "test" makes most sense.
1. Add the Source under test and the test file to 2 places the karma.conf.js file. The first is to the "files" property, which
includes any 3rd party libraries that the source is dependent on. The second is to the preprocessor in order to have the code included
in the code coverage calculations.
1. Run "karma start"

Karma does provide a [configuration to continually autowatch](https://karma-runner.github.io/6.3/config/configuration-file.html#autowatch) file changes and run the tests, that configuration is not included in this
project but could be easily implemented in the future.