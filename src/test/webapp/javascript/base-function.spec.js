const {test} = QUnit;

QUnit.module('hello world', hooks => {
    let jqdiv;

    // create an element for the function
    hooks.beforeEach(() => {
        const div = document.createElement('div');
        div.setAttribute('id', 'message');
        document.body.appendChild(div);
        jqdiv = $(div);
    });

    // clean up the element after the test
    hooks.afterEach(() => {
        jqdiv.remove();
    });

    test('write hello world', assert => {
        helloWorld();
        assert.equal($('#message').text(), 'Hello World!');
    });
});